## AWS Lambda Developer Guide
## References
* [AWS Severless Application Model (SAM)](https://aws.amazon.com/serverless/sam)
* [GitHub Repo](https://github.com/awsdocs/aws-lambda-developer-guide/tree/main/sample-apps/blank-python)
* [Blank Python]()
## Information From Web Site
This repository contains additional resources for the AWS Lambda developer guide.

- [iam-policies](./iam-policies) - Sample permissions policies for cross-service use cases.
- [sample-apps](./sample-apps) - Sample applications that demonstrate features and use cases for the AWS Lambda service and managed runtimes.
- [templates](./templates) - AWS CloudFormation templates for creating functions and VPC network resources.

## License Summary

The sample code within this repo is made available under a modified MIT license. See the [LICENSE](./LICENSE) file.
